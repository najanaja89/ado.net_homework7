﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_Homework7
{
    public class DataServices
    {
        public void ProductList(ProductContext context)
        {
            var listContext = context.Products.ToList();
            for (int i = 0; i < listContext.Count; i++)
            {
                Console.WriteLine($"prod. №{i}\nprod. name: {listContext.ElementAt(i).Name}\nprod. price: {listContext.ElementAt(i).Price}\nprod. count: {listContext.ElementAt(i).Count}");
            }
        }

        public void ProductEdit(ProductContext context, int j)
        {
            var productsListEdit = context.Products.ToList();
            int i = 0;
            foreach (var item in productsListEdit)
            {
                if (i == j)
                {
                    Console.WriteLine("Enter product name");
                    item.Name = Console.ReadLine();
                    Console.WriteLine("Enter product price");
                    item.Price = Console.ReadLine();
                    Console.WriteLine("Enter product count");
                    item.Count = Console.ReadLine();
                    context.SaveChanges();
                    Console.WriteLine("Product changed");
                    return;
                }
                i++;
            }
            Console.WriteLine("Product not found");
        }

        public void ProductDelete(ProductContext context, int j)
        {
            var productsListDelete = context.Products.ToList();
            for (int i = 0; i < productsListDelete.Count; i++)
            {
                if (i == j)
                {
                    context.Products.Remove(productsListDelete.ElementAt(i));
                    context.SaveChanges();
                    return;
                }
            }   
        }

    }
}
