﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_Homework7
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                using (var context = new ProductContext())
                {
                    string menu = "";
                    List<Product> products = new List<Product>();
                    DataServices dataServices = new DataServices();
                    Console.WriteLine("Press 1 to add Product to stock");
                    Console.WriteLine("Press 2 to edit Product in stock");
                    Console.WriteLine("Press 3 to delete Product from stock");
                    Console.WriteLine("Press 4 to view list of products ");
                    Console.WriteLine("Press 0 to exit ");
                    menu = Console.ReadLine();
                    switch (menu)
                    {
                        case "1":

                            Console.WriteLine("Enter product name");
                            string name = Console.ReadLine();
                            Console.WriteLine("Enter product price");
                            string price = Console.ReadLine();
                            Console.WriteLine("Enter product count");
                            string count = Console.ReadLine();
                            Product product = new Product(name, price, count);

                            context.Products.Add(product);
                            context.SaveChanges();
                            break;

                        case "2":
                            Console.WriteLine("Enter number of product to edit");
                            dataServices.ProductList(context);
                            int.TryParse(Console.ReadLine(), out int k);

                            dataServices.ProductEdit(context, k);

                            Console.ReadLine();
                            break;

                        case "3":
                            Console.WriteLine("Enter number of product to delete");
                            dataServices.ProductList(context);
                            int.TryParse(Console.ReadLine(), out int choosen);

                            dataServices.ProductDelete(context, choosen);

                            Console.ReadLine();
                            break;

                        case "4":
                            dataServices.ProductList(context);
                            Console.ReadLine();
                            break;

                        case "0":
                            System.Environment.Exit(0);
                            break;
                            
                        default:
                            break;
                    }

                }
            } 
        }
    }
}
