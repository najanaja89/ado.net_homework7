﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ado.net_Homework7
{
    public class ProductContext:DbContext
    {
        public ProductContext() : base("appConnection")
        { }

        public DbSet<Product> Products { get; set; }
    }
}
