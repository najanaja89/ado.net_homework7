﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_Homework7
{
    public class Product : Entity
    {
        public string Name { get; set; }
        public string Price { get; set; }
        public string Count { get; set; }
        public Product()
        {

        }
        public Product(string name, string price, string count)
        {
            Name = name;
            Price = price;
            Count = count;
        }
    }
}
